// -*- mode: c++ -*-
// Copyright 2016 Keyboardio, inc. <jesse@keyboard.io>
// See "LICENSE" for license details

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built"
#endif


/**
 * These #include directives pull in the Kaleidoscope firmware core,
 * as well as the Kaleidoscope plugins we use in the Model 01's firmware
 */
#include "bepo.h"
using namespace Bepo;

// The Kaleidoscope core
#include "Kaleidoscope.h"

// Support for storing the keymap in EEPROM
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"

// Support for communicating with the host via a simple Serial protocol
#include "Kaleidoscope-FocusSerial.h"

// Support for keys that move the mouse
#include "Kaleidoscope-MouseKeys.h"

// Support for macros
#include "Kaleidoscope-Macros.h"

// Support for controlling the keyboard's LEDs
#include "Kaleidoscope-LEDControl.h"

// Support for "Numpad" mode, which is mostly just the Numpad specific LED mode
#include "Kaleidoscope-NumPad.h"

// Support for the "Boot greeting" effect, which pulses the 'LED' button for 10s
// when the keyboard is connected to a computer (or that computer is powered on)
#include "Kaleidoscope-LEDEffect-BootGreeting.h"

// Support for LED modes that set all LEDs to a single color
#include "Kaleidoscope-LEDEffect-SolidColor.h"

// Support for an LED mode that makes all the LEDs 'breathe'
#include "Kaleidoscope-LEDEffect-Breathe.h"

// Support for an LED mode that makes a red pixel chase a blue pixel across the keyboard
#include "Kaleidoscope-LEDEffect-Chase.h"

// Support for LED modes that pulse the keyboard's LED in a rainbow pattern
#include "Kaleidoscope-LEDEffect-Rainbow.h"

// Support for an LED mode that lights up the keys as you press them
#include "Kaleidoscope-LED-Stalker.h"

// Support for an LED mode that prints the keys you press in letters 4px high
#include "Kaleidoscope-LED-AlphaSquare.h"

// Support for shared palettes for other plugins, like Colormap below
#include "Kaleidoscope-LED-Palette-Theme.h"

// Support for an LED mode that lets one configure per-layer color maps
#include "Kaleidoscope-Colormap.h"

// Support for Keyboardio's internal keyboard testing mode
#include "Kaleidoscope-HardwareTestMode.h"

// Support for host power management (suspend & wakeup)
#include "Kaleidoscope-HostPowerManagement.h"

// Support for magic combos (key chords that trigger an action)
#include "Kaleidoscope-MagicCombo.h"

// Support for USB quirks, like changing the key state report protocol
#include "Kaleidoscope-USB-Quirks.h"

// Ajouts manuels
#include <Kaleidoscope-Qukeys.h>
#include <Kaleidoscope-OneShot.h>
#include <Kaleidoscope-Escape-OneShot.h>
#include <Kaleidoscope-LED-ActiveModColor.h>
#include <Kaleidoscope-LEDEffect-DigitalRain.h>
#include "Kaleidoscope-LEDEffect-FunctionalColor.h"

/** This 'enum' is a list of all the macros used by the Model 01's firmware
  * The names aren't particularly important. What is important is that each
  * is unique.
  *
  * These are the names of your macros. They'll be used in two places.
  * The first is in your keymap definitions. There, you'll use the syntax
  * `M(MACRO_NAME)` to mark a specific keymap position as triggering `MACRO_NAME`
  *
  * The second usage is in the 'switch' statement in the `macroAction` function.
  * That switch statement actually runs the code associated with a macro when
  * a macro key is pressed.
  */
/*
enum { MACRO_VERSION_INFO,
       MACRO_ANY
     };
*/
/** The Model 01's key layouts are defined as 'keymaps'. By default, there are three
  * keymaps: The standard QWERTY keymap, the "Function layer" keymap and the "Numpad"
  * keymap.
  *
  * Each keymap is defined as a list using the 'KEYMAP_STACKED' macro, built
  * of first the left hand's layout, followed by the right hand's layout.
  *
  * Keymaps typically consist mostly of `Key_` definitions. There are many, many keys
  * defined as part of the USB HID Keyboard specification. You can find the names
  * (if not yet the explanations) for all the standard `Key_` defintions offered by
  * Kaleidoscope in these files:
  *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs_keyboard.h
  *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs_consumerctl.h
  *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs_sysctl.h
  *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs_keymaps.h
  *
  * Additional things that should be documented here include
  *   using ___ to let keypresses fall through to the previously active layer
  *   using XXX to mark a keyswitch as 'blocked' on this layer
  *   using ShiftToLayer() and LockLayer() keys to change the active keymap.
  *   keeping NUM and FN consistent and accessible on all layers
  *
  * The PROG key is special, since it is how you indicate to the board that you
  * want to flash the firmware. However, it can be remapped to a regular key.
  * When the keyboard boots, it first looks to see whether the PROG key is held
  * down; if it is, it simply awaits further flashing instructions. If it is
  * not, it continues loading the rest of the firmware and the keyboard
  * functions normally, with whatever binding you have set to PROG. More detail
  * here: https://community.keyboard.io/t/how-the-prog-key-gets-you-into-the-bootloader/506/8
  *
  * The "keymaps" data structure is a list of the keymaps compiled into the firmware.
  * The order of keymaps in the list is important, as the ShiftToLayer(#) and LockLayer(#)
  * macros switch to key layers based on this list.
  *
  *

  * A key defined as 'ShiftToLayer(FUNCTION)' will switch to FUNCTION while held.
  * Similarly, a key defined as 'LockLayer(NUMPAD)' will switch to NUMPAD when tapped.
  */

/**
  * Layers are "0-indexed" -- That is the first one is layer 0. The second one is layer 1.
  * The third one is layer 2.
  * This 'enum' lets us use names like QWERTY, FUNCTION, and NUMPAD in place of
  * the numbers 0, 1 and 2.
  *
  */

enum { BEPO, FONCTIONS, PAVE, QWERTY, BEPO_QWERTY, SOURIS, VIDE, }; // layers

enum { MACRO_VERSION_INFO, MACRO_ANY, MACRO_FCUP, MACRO_FCDOWN};

enum { COMBINAISON_BEPO, COMBINAISON_BEPO_QWERTY, COMBINAISON_SOURIS, COMBINAISON_LED_SUIVANTE, COMBINAISON_LED_PRECEDENTE };


/* This comment temporarily turns off astyle's indent enforcement
 *   so we can make the keymaps actually resemble the physical key layout better
 */
// *INDENT-OFF*

KEYMAPS(

  [BEPO] = KEYMAP_STACKED
  (Key_BP_Dollar,                Key_BP_GuillemetDouble,       Key_BP_GuillemetOuvrant,      Key_BP_GuillemetFermant,      Key_BP_ParentheseOuvrante,    Key_BP_ParentheseFermante,    Key_Escape,
   Key_BP_Pourcent,              Key_BP_B,                     Key_BP_EAigu,                 Key_BP_P,                     Key_BP_O,                     Key_BP_EGrave,                Key_Tab,
   Key_BP_W,                     Key_BP_A,                     Key_BP_U,                     Key_BP_I,                     Key_BP_E,                     Key_BP_Virgule,
   Key_BP_ECirconflexe,          Key_BP_AGrave,                Key_BP_Y,                     Key_BP_X,                     Key_BP_Point,                 Key_BP_K,                     Key_Enter,
   RALT_T(PcApplication),        Key_Spacebar,                 Key_LeftShift,                Key_LeftControl,
   OSL(FONCTIONS),

   MoveToLayer(QWERTY),          Key_BP_Arobase,               Key_BP_Plus,                  Key_BP_TraitDUnion,           Key_BP_Slash,                 Key_BP_Asterisque,            Key_BP_Egal,
   Key_Delete,                   Key_BP_Circonflexe,           Key_BP_V,                     Key_BP_D,                     Key_BP_L,                     Key_BP_J,                     Key_BP_Z,
                                 Key_BP_C,                     Key_BP_T,                     Key_BP_S,                     Key_BP_R,                     Key_BP_N,                     Key_BP_M,
   Key_Backspace,                Key_BP_Apostrophe,            Key_BP_Q,                     Key_BP_G,                     Key_BP_H,                     Key_BP_F,                     Key_BP_CCedille,
   Key_LeftAlt,                  Key_RightShift,               Key_Enter,                    RALT_T(PcApplication),
   OSL(PAVE)),

  [FONCTIONS] = KEYMAP_STACKED
  (Key_PrintScreen,              Key_F1,                       Key_F2,                       Key_F3,                       Key_F4,                       Key_F5,                       ___,
   LCTRL(Key_BP_R),              LCTRL(Key_BP_F),              Key_Home,                     Key_UpArrow,                  Key_End,                      Key_PageUp,                   ___,
   LCTRL(Key_BP_S),              LCTRL(Key_BP_P),              Key_LeftArrow,                Key_DownArrow,                Key_RightArrow,               Key_PageDown,
   LCTRL(Key_BP_A),              LCTRL(Key_BP_X),              LCTRL(Key_BP_C),              LCTRL(Key_BP_V),              LCTRL(Key_BP_Z),              LCTRL(Key_BP_Y),              ___,
   ___,                          ___,                          ___,                          Key_LeftGui,
   ___,
   
   ___,                          Key_F6,                       Key_F7,                       Key_F8,                       Key_F9,                       Key_F10,                      Key_F11,
   ___,                          Consumer_Mute,                Key_Home,                     Key_UpArrow,                  Key_End,                      Key_PageUp,                   Key_F12,
                                 Consumer_VolumeIncrement,     Key_LeftArrow,                Key_DownArrow,                Key_RightArrow,               Key_PageDown,                 Consumer_Repeat,
   ___,                          Consumer_VolumeDecrement,     Consumer_Rewind,              Consumer_PlaySlashPause,      Consumer_FastForward,         Consumer_StopSlashEject,      Consumer_Record,
   Key_RightGui,                 ___,                          ___,                          ___,
   ___),

  [PAVE] = KEYMAP_STACKED
  (XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          ___,
   Key_Sysreq,                   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          ___,
   System_Sleep,                 XXX,                          XXX,                          XXX,                          M(MACRO_FCUP),                Key_LEDEffectNext,
   System_PowerDown,             XXX,                          XXX,                          XXX,                          M(MACRO_FCDOWN),              Key_LEDEffectPrevious,        ___,
   ___,                          ___,                          ___,                          Key_LeftGui,
   ___,

   ___,                          XXX,                          Key_KeypadAdd,                Key_KeypadSubtract,           Key_KeypadDivide,             Key_KeypadMultiply,           XXX,           
   ___,                          Key_KeypadNumLock,            Key_Keypad7,                  Key_Keypad8,                  Key_Keypad9,                  Key_KeypadEquals,             XXX,
                                 Key_ScrollLock,               Key_Keypad4,                  Key_Keypad5,                  Key_Keypad6,                  Key_KeypadDot,                XXX,
   ___,                          Key_Insert,                   Key_Keypad1,                  Key_Keypad2,                  Key_Keypad3,                  Key_Keypad0,                  XXX,
   Key_RightGui,                 ___,                          ___,                          ___,
   ___),

  [QWERTY] =  KEYMAP_STACKED
  (Key_BP_Dollar,                Key_BP_GuillemetDouble,       Key_BP_GuillemetOuvrant,      Key_BP_GuillemetFermant,      Key_BP_ParentheseOuvrante,    Key_BP_ParentheseFermante,    ___,
   Key_BP_Apostrophe,            Key_BP_Q,                     Key_BP_W,                     Key_BP_E,                     Key_BP_R,                     Key_BP_T,                     ___,
   Key_BP_Pourcent,              Key_BP_A,                     Key_BP_S,                     Key_BP_D,                     Key_BP_F,                     Key_BP_G,
   Key_BP_Egal,                  Key_BP_Z,                     Key_BP_X,                     Key_BP_C,                     Key_BP_V,                     Key_BP_B,                     ___,
   ___,                          ___,                          ___,                          ___,
   ___,

   MoveToLayer(BEPO),            Key_BP_Arobase,               Key_BP_Plus,                  Key_BP_TraitDUnion,           Key_BP_Slash,                 Key_BP_Asterisque,            XXX,           
   ___,                          Key_BP_Y,                     Key_BP_U,                     Key_BP_I,                     Key_BP_O,                     Key_BP_P,                     XXX,
                                 Key_BP_H,                     Key_BP_J,                     Key_BP_K,                     Key_BP_L,                     Key_BP_Apostrophe,            Key_BP_Circonflexe,
   ___,                          Key_BP_N,                     Key_BP_M,                     Key_BP_Virgule,               Key_BP_Point,                 Key_BP_Slash,                 XXX,
   ___,                          ___,                          ___,                          ___,
   ___),

  [BEPO_QWERTY] =  KEYMAP_STACKED
  (Key_Backtick,                 Key_1,                        Key_2,                        Key_3,                        Key_4,                        Key_5,                        ___,
   Key_Equals,                   Key_B,                        Key_E,                        Key_P,                        Key_O,                        Key_E,                        ___,
   Key_W,                        Key_A,                        Key_U,                        Key_I,                        Key_E,                        Key_Comma,
   Key_A,                        Key_E,                        Key_Y,                        Key_X,                        Key_Period,                   Key_K,                        ___,
   ___,                          ___,                          ___,                          ___,
   ___,

   MoveToLayer(BEPO),            Key_6,                        Key_7,                        Key_8,                        Key_9,                        Key_0,                        Key_Minus,
   ___,                          Key_Semicolon,                Key_V,                        Key_D,                        Key_L,                        Key_J,                        Key_Z,
                                 Key_C,                        Key_T,                        Key_S,                        Key_R,                        Key_N,                        Key_M,
   ___,                          Key_Slash,                    Key_Q,                        Key_G,                        Key_H,                        Key_F,                        Key_C,
   ___,                          ___,                          ___,                          ___,
   ___),
   
  [SOURIS] = KEYMAP_STACKED
  (XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          ___,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          ___,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          ___,
   ___,                          ___,                          ___,                          ___,
   ___,

   MoveToLayer(BEPO),            XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,           
   ___,                          Key_mouseWarpNW,              XXX,                          Key_mouseUp,                  XXX,                          Key_mouseWarpNE,              XXX,
                                 XXX,                          Key_mouseL,                   Key_mouseDn,                  Key_mouseR,                   XXX,                          XXX,
   ___,                          Key_mouseWarpSW,              Key_mouseBtnL,                Key_mouseBtnM,                Key_mouseBtnR,                Key_mouseWarpSE,              XXX,
   ___,                          ___,                          ___,                          ___,
   ___),
   
  [VIDE] =  KEYMAP_STACKED
  (XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,
   XXX,

   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,           
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
                                 XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,                          XXX,
   XXX,                          XXX,                          XXX,                          XXX,
   XXX),
	) // KEYMAPS(

/* Re-enable astyle's indent enforcement */
// *INDENT-ON*

/** versionInfoMacro handles the 'firmware version info' macro
 *  When a key bound to the macro is pressed, this macro
 *  prints out the firmware build information as virtual keystrokes
 */

static void versionInfoMacro(uint8_t keyState) {
  if (keyToggledOn(keyState)) {
    Macros.type(PSTR("Keyboardio Model 01 - Kaleidoscope "));
    Macros.type(PSTR(BUILD_INFORMATION));
  }
}

/** anyKeyMacro is used to provide the functionality of the 'Any' key.
 *
 * When the 'any key' macro is toggled on, a random alphanumeric key is
 * selected. While the key is held, the function generates a synthetic
 * keypress event repeating that randomly selected key.
 *
 */

static void anyKeyMacro(uint8_t keyState) {
  static Key lastKey;
  bool toggledOn = false;
  if (keyToggledOn(keyState)) {
    lastKey.setKeyCode(Key_A.getKeyCode() + (uint8_t)(millis() % 36));
    toggledOn = true;
  }

  if (keyIsPressed(keyState))
    Kaleidoscope.hid().keyboard().pressKey(lastKey, toggledOn);
}


/** macroAction dispatches keymap events that are tied to a macro
    to that macro. It takes two uint8_t parameters.

    The first is the macro being called (the entry in the 'enum' earlier in this file).
    The second is the state of the keyswitch. You can use the keyswitch state to figure out
    if the key has just been toggled on, is currently pressed or if it's just been released.

    The 'switch' statement should have a 'case' for each entry of the macro enum.
    Each 'case' statement should call out to a function to handle the macro in question.

 */

const macro_t *macroAction(uint8_t macroIndex, uint8_t keyState) {
  switch (macroIndex) {

  case MACRO_VERSION_INFO:
    versionInfoMacro(keyState);
    break;

  case MACRO_ANY:
    anyKeyMacro(keyState);
    break;

  case MACRO_FCUP:
    FunctionalColor::brightnessUp(keyState);
    break;
   
  case MACRO_FCDOWN:
    FunctionalColor::brightnessDown(keyState);
    break;
  }
  return MACRO_NONE;
}



// These 'solid' color effect definitions define a rainbow of
// LED color modes calibrated to draw 500mA or less on the
// Keyboardio Model 01.


static kaleidoscope::plugin::LEDSolidColor solidRed(160, 0, 0);
static kaleidoscope::plugin::LEDSolidColor solidOrange(140, 70, 0);
static kaleidoscope::plugin::LEDSolidColor solidYellow(130, 100, 0);
static kaleidoscope::plugin::LEDSolidColor solidGreen(0, 160, 0);
static kaleidoscope::plugin::LEDSolidColor solidBlue(0, 70, 130);
static kaleidoscope::plugin::LEDSolidColor solidIndigo(0, 0, 170);
static kaleidoscope::plugin::LEDSolidColor solidViolet(130, 0, 120);


using namespace kaleidoscope::plugin::LEDFunctionalColor;

FC_START_COLOR_LIST(couleursPerso)
 // Use any number of FCGROUPKEYs above a FC_KEYCOLOR to set several keys to the same color
 FC_GROUPKEY(Key_BP_B)
 FC_GROUPKEY(Key_BP_EAigu)
 FC_GROUPKEY(Key_BP_P)
 FC_GROUPKEY(Key_BP_O)
 FC_GROUPKEY(Key_BP_EGrave)
 FC_GROUPKEY(Key_BP_W)
 FC_GROUPKEY(Key_BP_A)
 FC_GROUPKEY(Key_BP_U)
 FC_GROUPKEY(Key_BP_I)
 FC_GROUPKEY(Key_BP_E)
 FC_GROUPKEY(Key_BP_ECirconflexe)
 FC_GROUPKEY(Key_BP_AGrave)
 FC_GROUPKEY(Key_BP_Y)
 FC_GROUPKEY(Key_BP_X)
 FC_GROUPKEY(Key_BP_K)
 FC_GROUPKEY(Key_BP_V)
 FC_GROUPKEY(Key_BP_D)
 FC_GROUPKEY(Key_BP_L)
 FC_GROUPKEY(Key_BP_J)
 FC_GROUPKEY(Key_BP_Z)
 FC_GROUPKEY(Key_BP_C)
 FC_GROUPKEY(Key_BP_T)
 FC_GROUPKEY(Key_BP_S)
 FC_GROUPKEY(Key_BP_R)
 FC_GROUPKEY(Key_BP_N)
 FC_GROUPKEY(Key_BP_M)
 FC_GROUPKEY(Key_BP_Q)
 FC_GROUPKEY(Key_BP_G)
 FC_GROUPKEY(Key_BP_H)
 FC_GROUPKEY(Key_BP_F)
 FC_KEYCOLOR(Key_BP_CCedille, steelblue)
 FC_GROUPKEY(Key_BP_GuillemetDouble)
 FC_GROUPKEY(Key_BP_GuillemetOuvrant)
 FC_GROUPKEY(Key_BP_GuillemetFermant)
 FC_GROUPKEY(Key_BP_ParentheseOuvrante)
 FC_GROUPKEY(Key_BP_ParentheseFermante)
 FC_GROUPKEY(Key_BP_Virgule)
 FC_GROUPKEY(Key_BP_Point)
 FC_GROUPKEY(Key_BP_Circonflexe)
 FC_KEYCOLOR(Key_BP_Apostrophe, seagreen)
 FC_GROUPKEY(Key_BP_Dollar)
 FC_GROUPKEY(Key_BP_Pourcent)
 FC_GROUPKEY(Key_BP_Arobase)
 FC_GROUPKEY(Key_BP_Plus)
 FC_GROUPKEY(Key_BP_TraitDUnion)
 FC_GROUPKEY(Key_BP_Slash)
 FC_GROUPKEY(Key_BP_Asterisque)
 FC_GROUPKEY(Key_BP_Egal)
 FC_GROUPKEY(Key_Spacebar)
 FC_KEYCOLOR(Key_Enter, turquoise)
 FC_GROUPKEY(Key_UpArrow)
 FC_GROUPKEY(Key_DownArrow)
 FC_GROUPKEY(Key_LeftArrow)
 FC_KEYCOLOR(Key_RightArrow, aqua)
 FC_GROUPKEY(Key_PageUp)
 FC_GROUPKEY(Key_PageDown)
 FC_GROUPKEY(Key_Home)
 FC_KEYCOLOR(Key_End, forestgreen)
 FC_GROUPKEY(Key_PrintScreen)
 FC_GROUPKEY(LCTRL(Key_BP_R))
 FC_GROUPKEY(LCTRL(Key_BP_F))
 FC_GROUPKEY(LCTRL(Key_BP_S))
 FC_GROUPKEY(LCTRL(Key_BP_P))
 FC_GROUPKEY(LCTRL(Key_BP_A))
 FC_GROUPKEY(LCTRL(Key_BP_X))
 FC_GROUPKEY(LCTRL(Key_BP_C))
 FC_GROUPKEY(LCTRL(Key_BP_V))
 FC_GROUPKEY(LCTRL(Key_BP_Z))
 FC_KEYCOLOR(LCTRL(Key_BP_Y), purple)
 FC_GROUPKEY(Consumer_Mute)
 FC_GROUPKEY(Consumer_VolumeIncrement)
 FC_GROUPKEY(Consumer_VolumeDecrement)
 FC_GROUPKEY(Consumer_Rewind)
 FC_GROUPKEY(Consumer_PlaySlashPause)
 FC_GROUPKEY(Consumer_FastForward)
 FC_GROUPKEY(Consumer_StopSlashEject)
 FC_GROUPKEY(Consumer_Repeat)
 FC_KEYCOLOR(Consumer_Record, darkred)
 FC_GROUPKEY(Key_F1)
 FC_GROUPKEY(Key_F2)
 FC_GROUPKEY(Key_F3)
 FC_GROUPKEY(Key_F4)
 FC_GROUPKEY(Key_F5)
 FC_GROUPKEY(Key_F6)
 FC_GROUPKEY(Key_F7)
 FC_GROUPKEY(Key_F8)
 FC_GROUPKEY(Key_F9)
 FC_GROUPKEY(Key_F10)
 FC_GROUPKEY(Key_F11)
 FC_KEYCOLOR(Key_F12, darkblue)
 FC_GROUPKEY(Key_Keypad1)
 FC_GROUPKEY(Key_Keypad2)
 FC_GROUPKEY(Key_Keypad3)
 FC_GROUPKEY(Key_Keypad4)
 FC_GROUPKEY(Key_Keypad5)
 FC_GROUPKEY(Key_Keypad6)
 FC_GROUPKEY(Key_Keypad7)
 FC_GROUPKEY(Key_Keypad8)
 FC_GROUPKEY(Key_Keypad9)
 FC_KEYCOLOR(Key_Keypad0, chartreuse)
 FC_GROUPKEY(Key_KeypadAdd)
 FC_GROUPKEY(Key_KeypadSubtract)
 FC_GROUPKEY(Key_KeypadDivide)
 FC_GROUPKEY(Key_KeypadMultiply)
 FC_GROUPKEY(Key_KeypadEquals)
 FC_KEYCOLOR(Key_KeypadDot, aquamarine)
 FC_GROUPKEY(Key_KeypadNumLock)
 FC_GROUPKEY(Key_ScrollLock)
 FC_KEYCOLOR(Key_Insert, coral)
 FC_GROUPKEY(M(MACRO_FCUP))
 FC_KEYCOLOR(M(MACRO_FCDOWN), lightseagreen)
 FC_GROUPKEY(Key_LEDEffectNext)
 FC_KEYCOLOR(Key_LEDEffectPrevious, limegreen)
 FC_GROUPKEY(Key_Sysreq)
 FC_GROUPKEY(System_Sleep)
 FC_KEYCOLOR(System_PowerDown, red)
 FC_GROUPKEY(RALT_T(PcApplication))
 FC_GROUPKEY(Key_LeftShift)
 FC_GROUPKEY(Key_LeftControl)
 FC_GROUPKEY(Key_LeftAlt)
 FC_KEYCOLOR(Key_RightShift, indigo)
 FC_GROUPKEY(Key_Escape)
 FC_KEYCOLOR(Key_Tab, mediumturquoise)
 FC_GROUPKEY(Key_Delete)
 FC_KEYCOLOR(Key_Backspace, magenta)
 FC_GROUPKEY(OSL(FONCTIONS))
 FC_GROUPKEY(OSL(PAVE))
 FC_KEYCOLOR(MoveToLayer(QWERTY), lime)
FC_END_COLOR_LIST

FunctionalColor funColorCouleursPerso(FC_COLOR_LIST(couleursPerso), 220, false);

/** toggleLedsOnSuspendResume toggles the LEDs off when the host goes to sleep,
 * and turns them back on when it wakes up.
 */
void toggleLedsOnSuspendResume(kaleidoscope::plugin::HostPowerManagement::Event event) {
  switch (event) {
  case kaleidoscope::plugin::HostPowerManagement::Suspend:
    LEDControl.disable();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Resume:
    LEDControl.enable();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Sleep:
    break;
  }
}

/** hostPowerManagementEventHandler dispatches power management events (suspend,
 * resume, and sleep) to other functions that perform action based on these
 * events.
 */
void hostPowerManagementEventHandler(kaleidoscope::plugin::HostPowerManagement::Event event) {
  toggleLedsOnSuspendResume(event);
}

/** This 'enum' is a list of all the magic combos used by the Model 01's
 * firmware The names aren't particularly important. What is important is that
 * each is unique.
 *
 * These are the names of your magic combos. They will be used by the
 * `USE_MAGIC_COMBOS` call below.
 */
enum {
  // Toggle between Boot (6-key rollover; for BIOSes and early boot) and NKRO
  // mode.
  COMBO_TOGGLE_NKRO_MODE,
  // Enter test mode
  COMBO_ENTER_TEST_MODE,
  // Niveau BEPO_QWERTY
  COMBO_BEPO_QWERTY,
  // Niveau QWERTY
  COMBO_QWERTY
};

/** Wrappers, to be used by MagicCombo. **/

/**
 * This simply toggles the keyboard protocol via USBQuirks, and wraps it within
 * a function with an unused argument, to match what MagicCombo expects.
 */
static void toggleKeyboardProtocol(uint8_t combo_index) {
  USBQuirks.toggleKeyboardProtocol();
}

/**
 *  This enters the hardware test mode
 */
static void enterHardwareTestMode(uint8_t combo_index) {
  HardwareTestMode.runTests();
}

void passer_au_niveau_bepo_qwerty(uint8_t combo_index) {
  Layer.activate(BEPO_QWERTY);
}

void passer_au_niveau_qwerty(uint8_t combo_index) {
  Layer.activate(QWERTY);
}

/** Magic combo list, a list of key combo and action pairs the firmware should
 * recognise.
 */
USE_MAGIC_COMBOS({.action = toggleKeyboardProtocol,
                  // Left Fn + Esc + Shift
                  .keys = { R3C6, R2C6, R3C7 }
}, {
  .action = enterHardwareTestMode,
  // Left Fn + Prog + LED
  .keys = { R3C6, R0C0, R0C6 }
}, {
  .action = passer_au_niveau_bepo_qwerty,
  // Left Fn + Right Fn + Led
  .keys = {R3C6, R3C9, R0C6}
}, {
  .action = passer_au_niveau_qwerty,
  // Left Fn + Right Fn + Any
  .keys = {R3C6, R3C9, R0C9}
},);

// First, tell Kaleidoscope which plugins you want to use.
// The order can be important. For example, LED effects are
// added in the order they're listed here.
KALEIDOSCOPE_INIT_PLUGINS(
  // The EEPROMSettings & EEPROMKeymap plugins make it possible to have an
  // editable keymap in EEPROM.
  EEPROMSettings,
  EEPROMKeymap,
  Qukeys,
  OneShot,
  EscapeOneShot,
  MagicCombo,
  ActiveModColorEffect,
  
  // Focus allows bi-directional communication with the host, and is the
  // interface through which the keymap in EEPROM can be edited.
  Focus,

  // FocusSettingsCommand adds a few Focus commands, intended to aid in
  // changing some settings of the keyboard, such as the default layer (via the
  // `settings.defaultLayer` command)
  FocusSettingsCommand,

  // FocusEEPROMCommand adds a set of Focus commands, which are very helpful in
  // both debugging, and in backing up one's EEPROM contents.
  FocusEEPROMCommand,

  // The boot greeting effect pulses the LED button for 10 seconds after the
  // keyboard is first connected
  BootGreetingEffect,

  // The hardware test mode, which can be invoked by tapping Prog, LED and the
  // left Fn button at the same time.
  HardwareTestMode,

  // LEDControl provides support for other LED modes
  LEDControl,

  // We start with the LED effect that turns off all the LEDs.
  LEDOff,

  // Couleurs fonctionnelles.
  funColorCouleursPerso,

  // The rainbow effect changes the color of all of the keyboard's keys at the same time
  // running through all the colors of the rainbow.
  //LEDRainbowEffect,

  // The rainbow wave effect lights up your keyboard with all the colors of a rainbow
  // and slowly moves the rainbow across your keyboard
  LEDRainbowWaveEffect,

  // Effet pluie Matrix.  
  LEDDigitalRainEffect,

  // The chase effect follows the adventure of a blue pixel which chases a red pixel across
  // your keyboard. Spoiler: the blue pixel never catches the red pixel
  //LEDChaseEffect,

  // These static effects turn your keyboard's LEDs a variety of colors
  //solidRed, solidOrange, solidYellow, solidGreen, solidBlue, solidIndigo, solidViolet,

  // The breathe effect slowly pulses all of the LEDs on your keyboard
  //LEDBreatheEffect,

  // The AlphaSquare effect prints each character you type, using your
  // keyboard's LEDs as a display
  //AlphaSquareEffect,

  // The stalker effect lights up the keys you've pressed recently
  //StalkerEffect,
  // The LED Palette Theme plugin provides a shared palette for other plugins,
  // like Colormap below
  LEDPaletteTheme,

  // The Colormap effect makes it possible to set up per-layer colormaps
  //ColormapEffect,
  // The numpad plugin is responsible for lighting up the 'numpad' mode
  // with a custom LED effect  
  //NumPad,

  // The macros plugin adds support for macros
  Macros,

  // The MouseKeys plugin lets you add keys to your keymap which move the mouse.
  //MouseKeys,

  // The HostPowerManagement plugin allows us to turn LEDs off when then host
  // goes to sleep, and resume them when it wakes up.
  HostPowerManagement,

  // The MagicCombo plugin lets you use key combinations to trigger custom
  // actions - a bit like Macros, but triggered by pressing multiple keys at the
  // same time.
  MagicCombo,

  // The USBQuirks plugin lets you do some things with USB that we aren't
  // comfortable - or able - to do automatically, but can be useful
  // nevertheless. Such as toggling the key report protocol between Boot (used
  // by BIOSes) and Report (NKRO).
  USBQuirks
);

/** The 'setup' function is one of the two standard Arduino sketch functions.
 * It's called when your keyboard first powers up. This is where you set up
 * Kaleidoscope and any plugins.
 */
void setup() {
  // First, call Kaleidoscope's internal setup function
  Kaleidoscope.setup();

  Qukeys.setHoldTimeout(150);
  Qukeys.setOverlapThreshold(50);
  OneShot.hold_time_out = 150;

  //LEDDigitalRainEffect.DROP_TICKS = 22;
  
  // While we hope to improve this in the future, the NumPad plugin
  // needs to be explicitly told which keymap layer is your numpad layer
  //NumPad.numPadLayer = PAVE;
  //NumPad.color = CRGB(0, 0, 160); // a blue color
  //NumPad.lock_hue = 85; // green

  // We configure the AlphaSquare effect to use RED letters
  //AlphaSquare.color = CRGB(255, 0, 0);

  // We set the brightness of the rainbow effects to 150 (on a scale of 0-255)
  // This draws more than 500mA, but looks much nicer than a dimmer effect
  //LEDRainbowEffect.brightness(150);
  LEDRainbowWaveEffect.brightness(150);

  // Set the action key the test mode should listen for to Left Fn
  HardwareTestMode.setActionKey(R3C6);

  // The LED Stalker mode has a few effects. The one we like is called
  // 'BlazingTrail'. For details on other options, see
  // https://github.com/keyboardio/Kaleidoscope/blob/master/docs/plugins/LED-Stalker.md
  StalkerEffect.variant = STALKER(BlazingTrail);

  // We want to make sure that the firmware starts with LED effects off
  // This avoids over-taxing devices that don't have a lot of power to share
  // with USB devices
  LEDOff.activate();

  // To make the keymap editable without flashing new firmware, we store
  // additional layers in EEPROM. For now, we reserve space for five layers. If
  // one wants to use these layers, just set the default layer to one in EEPROM,
  // by using the `settings.defaultLayer` Focus command, or by using the
  // `keymap.onlyCustom` command to use EEPROM layers only.
  EEPROMKeymap.setup(5);

  // We need to tell the Colormap plugin how many layers we want to have custom
  // maps for. To make things simple, we set it to five layers, which is how
  // many editable layers we have (see above).
  //ColormapEffect.max_layers(5);
}

/** loop is the second of the standard Arduino sketch functions.
  * As you might expect, it runs in a loop, never exiting.
  *
  * For Kaleidoscope-based keyboard firmware, you usually just want to
  * call Kaleidoscope.loop(); and not do anything custom here.
  */

void loop() {
  Kaleidoscope.loop();
}
